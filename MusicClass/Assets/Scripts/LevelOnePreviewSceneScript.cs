﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelOnePreviewSceneScript : MonoBehaviour {

    public static int number = 1;
    public static int score;
    public static int streak; 

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelTwoGameSceneScript : MonoBehaviour
{

    int number;
    public static bool answered;
    public static bool result;
    public static string answer;

    public Canvas PopUpCanvas;
    public Text ResultText;
    public Button BackButton;
    public Button TryAgainButton;

    public Text Question;
    public Image Note1;
    public Image Note2;
    public Image Note3;
    public Image Note4;
    public Text StandText1;
    public Text StandText2;
    public Text StandText3;
    public Text StandText4;

    // Use this for initialization
    void Start()
    {
        PopUpCanvas.enabled = false;
        answered = false;
        number = LevelTwoPreviewSceneScript.number;
        GetQuestion(number);
    }

    // Update is called once per frame
    void Update()
    {
        if (answered)
        {
            if (number < 6 && LevelTwoPreviewSceneScript.streak < 3)
            {
                LevelTwoPreviewSceneScript.number++;
                ReloadPage();
            }
            else
            {
                ResultText.text = "You've got " + LevelTwoPreviewSceneScript.score.ToString() + " correct answers! \n Congratulations!";
                PopUpCanvas.enabled = true;
            }
        }
    }

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void TryAgain()
    {
        LevelTwoPreviewSceneScript.number = 1;
        LevelTwoPreviewSceneScript.score = 0;
        SceneManager.LoadScene("LevelTwoGameScene");
    }

    public void ReloadPage()
    {
        SceneManager.LoadScene("LevelTwoGameScene");
    }

    public void GetQuestion(int number)
    {
        if (number == 1)
        {
            Question.text = "What do these symbols mean?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question1/play a half step higher");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question1/cancels a sharp or flat");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question1/play a half step lower");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question1/play the note loudly");
            StandText4.text = "play a half step higher";
            StandText1.text = "cancels a sharp or flat";
            StandText3.text = "play a half step lower";
            StandText2.text = "play the note loudly";

            answer = "2431";
        }
        else if (number == 2)
        {
            Question.text = "Which interval?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question2/5th");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question2/2nd");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question2/4th");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question2/3rd");
            StandText1.text = "5th";
            StandText3.text = "2nd";
            StandText4.text = "4th";
            StandText2.text = "3rd";

            answer = "1423";
        }
        else if (number == 3)
        {
            Question.text = "Which interval?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question3/5th");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question3/2nd");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question3/4th");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question3/6th");
            StandText2.text = "5th";
            StandText1.text = "2nd";
            StandText3.text = "4th";
            StandText4.text = "6th";

            answer = "2134";
        }
        else if (number == 4)
        {
            Question.text = "Which key signature?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question4/D Major");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question4/C Major");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question4/F Major");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question4/G Major");
            StandText3.text = "D Major";
            StandText4.text = "C Major";
            StandText1.text = "F Major";
            StandText2.text = "G Major";

            answer = "3142";
        }
        else if (number == 5)
        {
            Question.text = "Which key signature?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question5/B minor");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question5/A minor");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question5/D minor");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question5/E minor");
            StandText4.text = "B minor";
            StandText3.text = "A minor";
            StandText1.text = "D minor";
            StandText2.text = "E minor";

            answer = "3412";
        }
        else if (number == 6)
        {
            Question.text = "What is the symbol?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level2/question6/cut time");
            Note2.sprite = Resources.Load<Sprite>("notes/level2/question6/common time");
            Note3.sprite = Resources.Load<Sprite>("notes/level2/question6/natural sign");
            Note4.sprite = Resources.Load<Sprite>("notes/level2/question6/sharp sign");
            StandText4.text = "cut time";
            StandText3.text = "common time";
            StandText2.text = "natural sign";
            StandText1.text = "sharp sign";

            answer = "4321";
        }
    }
}

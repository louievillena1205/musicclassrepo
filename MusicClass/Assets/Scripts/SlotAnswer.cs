﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotAnswer : MonoBehaviour, IHasChanged
{
    [SerializeField] Transform slots;
    [SerializeField] Text Question;

    // Use this for initialization
    void Start () {
        HasChanged();
    }

    public void HasChanged()
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach (Transform slotTransform in slots)
        {
            GameObject item = slotTransform.GetComponent<Slot>().item;
            if (item)
            {
                builder.Append(item.name);
            }
        }
        if (builder.Length == 4)
        {
            if (LevelOneGameSceneScript.answer == builder.ToString())
            {
                Question.text = "GOOD JOB!";
                LevelOnePreviewSceneScript.streak++;
                LevelOnePreviewSceneScript.score++;
                StartCoroutine(DelayFunction());
            }
            else
            {
                Question.text = "TRY AGAIN";
                LevelOnePreviewSceneScript.streak = 0;
            }
        }
    }

    IEnumerator DelayFunction()
    {
        yield return new WaitForSeconds(1.5f);

        LevelOneGameSceneScript.answered = true;
    }
}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
    }
}

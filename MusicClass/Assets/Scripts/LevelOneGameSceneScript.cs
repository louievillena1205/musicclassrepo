﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelOneGameSceneScript : MonoBehaviour {
    
    int number;
    public static bool answered;
    public static bool result;
    public static string answer;

    public Canvas PopUpCanvas;
    public Text ResultText;
    public Button BackButton;
    public Button TryAgainButton;

    public Text Question;
    public Image Note1;
    public Image Note2;
    public Image Note3;
    public Image Note4;
    public Text StandText1;
    public Text StandText2;
    public Text StandText3;
    public Text StandText4;

    // Use this for initialization
    void Start () {
        PopUpCanvas.enabled = false;
        answered = false;
        number = LevelOnePreviewSceneScript.number;
        GetQuestion(number);
	}
	
	// Update is called once per frame
	void Update () {
        if (answered)
        {
            if (number < 9 && LevelOnePreviewSceneScript.streak < 3)
            {
                LevelOnePreviewSceneScript.number++;
                ReloadPage();
            } else
            {
                ResultText.text = "You've got " + LevelOnePreviewSceneScript.score.ToString() + " correct answers! \n Congratulations!";
                PopUpCanvas.enabled = true;
            }
        }
	}

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void TryAgain()
    {
        LevelOnePreviewSceneScript.number = 1;
        LevelOnePreviewSceneScript.score = 0;
        SceneManager.LoadScene("LevelOneGameScene");
    }

    public void ReloadPage()
    {
        SceneManager.LoadScene("LevelOneGameScene");
    }

    public void GetQuestion(int number)
    {
        if (number == 1)
        {
            Question.text = "What do these rests mean?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question1/silence for 1 beat");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question1/silence for 2 beats");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question1/silence for a whole measure");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question1/silence for half beat");
            StandText1.text = "silence for 1 beat";
            StandText2.text = "silence for 2 beats";
            StandText3.text = "silence for a whole measure";
            StandText4.text = "silence for half beat";

            answer = "1234";
        }
        else if (number == 2)
        {
            Question.text = "What do these dynamics mean?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question2/loud");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question2/soft");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question2/moderately loud");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question2/really soft");
            StandText4.text = "loud";
            StandText3.text = "soft";
            StandText2.text = "moderately loud";
            StandText1.text = "really soft";

            answer = "4321";
        }
        else if (number == 3)
        {
            Question.text = "What do these symbols mean?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question3/hold the note a little longer");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question3/repeat");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question3/gradually get louder");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question3/the end of the piece");
            StandText2.text = "hold the note a little longer";
            StandText3.text = "repeat";
            StandText1.text = "gradually get louder";
            StandText4.text = "the end of the piece";

            answer = "3124";
        }
        else if (number == 4)
        {
            Question.text = "What kind of rests?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question4/half rest");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question4/whole rest");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question4/quarter rest");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question4/eighth rest");
            StandText3.text = "half rest";
            StandText4.text = "whole rest";
            StandText1.text = "quarter rest";
            StandText2.text = "eighth rest";

            answer = "3412";
        }
        else if (number == 5)
        {
            Question.text = "What do these notes mean?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question5/1 beat");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question5/2 beats");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question5/4 beats");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question5/half beat");
            StandText1.text = "1 beat";
            StandText4.text = "2 beats";
            StandText3.text = "4 beats";
            StandText2.text = "half beat";

            answer = "1432";
        }
        else if (number == 6)
        {
            Question.text = "What kind of notes?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question6/quarter note");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question6/half note");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question6/whole note");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question6/eighth note");
            StandText2.text = "quarter note";
            StandText1.text = "half note";
            StandText4.text = "whole note";
            StandText3.text = "eighth note";

            answer = "2143";
        }
        else if (number == 7)
        {
            Question.text = "What is the symbol?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question7/flat");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question7/grand staff");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question7/fermata");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question7/time signature");
            StandText3.text = "flat";
            StandText2.text = "grand staff";
            StandText4.text = "fermata";
            StandText1.text = "time signature";

            answer = "4213";
        }
        else if (number == 8)
        {
            Question.text = "What is the symbol?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question8/bass clef");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question8/treble clef");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question8/tie");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question8/double bar line");
            StandText4.text = "bass clef";
            StandText1.text = "treble clef";
            StandText3.text = "tie";
            StandText2.text = "double bar line";

            answer = "2431";
        }
        else if (number == 9)
        {
            Question.text = "What is the symbol?";
            Note1.name = "1";
            Note2.name = "2";
            Note3.name = "3";
            Note4.name = "4";
            Note1.sprite = Resources.Load<Sprite>("notes/level1/question9/crescendo");
            Note2.sprite = Resources.Load<Sprite>("notes/level1/question9/bar line");
            Note3.sprite = Resources.Load<Sprite>("notes/level1/question9/tie");
            Note4.sprite = Resources.Load<Sprite>("notes/level1/question9/accent");
            StandText1.text = "crescendo";
            StandText4.text = "bar line";
            StandText2.text = "tie";
            StandText3.text = "accent";

            answer = "1342";
        }
    }
}

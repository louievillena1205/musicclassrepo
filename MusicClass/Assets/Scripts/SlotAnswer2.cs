﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotAnswer2 : MonoBehaviour, IHasChanged
{
    [SerializeField] Transform slots;
    [SerializeField] Text Question;

    // Use this for initialization
    void Start()
    {
        HasChanged();
    }

    public void HasChanged()
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach (Transform slotTransform in slots)
        {
            GameObject item = slotTransform.GetComponent<Slot>().item;
            if (item)
            {
                builder.Append(item.name);
            }
        }
        if (builder.Length == 4)
        {
            if (LevelTwoGameSceneScript.answer == builder.ToString())
            {
                Question.text = "GOOD JOB!";
                LevelTwoPreviewSceneScript.streak++;
                LevelTwoPreviewSceneScript.score++;
                StartCoroutine(DelayFunction());
            }
            else
            {
                Question.text = "TRY AGAIN";
                LevelTwoPreviewSceneScript.streak = 0;
            }
        }
    }

    IEnumerator DelayFunction()
    {
        yield return new WaitForSeconds(1.5f);

        LevelTwoGameSceneScript.answered = true;
    }
}
